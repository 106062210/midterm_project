function initwindow() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            user_email = user.email;
            var userid=user_email.replace(/\./g,"-");
            var useronline = firebase.database().ref('userid').child(userid).update({
                email:user_email,
                onlineState: true
            });
            var useroffline = firebase.database().ref('userid').child(userid).onDisconnect().update({
                email:user_email,
                onlineState: false
            });
            var Submit = document.getElementById("submit");
            var femail = document.getElementById("friendEmail");
            var modal = document.getElementById('myModal');
            var menu = document.getElementById('dynamic-menu');
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            //log out
            logout_button.addEventListener('click', function () {
                userid=user_email.replace(/\./g,"-");
                var userlogout = firebase.database().ref('userid').child(userid).set({
                    email:user_email,
                    onlineState: false
                });
                firebase.auth().signOut().then(function () {
                        window.location = "index.html";
                    })
                    .catch(function (error) {
                        alert('log out failed');
                    });
            });
            //add friend
            Submit.addEventListener('click',function(){
                if (femail.value != "") {
                    var fid = femail.value.replace(/\./g,"-");
                    var ref = firebase.database().ref(userid);
                    ref.once("value").then(function(snapshot){
                        if(snapshot.child(fid).exists()){
                            alert('already friend');
                            femail.value = "";
                            modal.style.display = "none";
                        }
                        else{
                            var userexist = firebase.database().ref('userid');
                            userexist.once("value").then(function(snapshot){
                                if(snapshot.child(fid).exists()){
                                    var user1friend = firebase.database().ref(userid).child(fid).set({
                                        email:femail.value
                                    });
                                    var user2friend = firebase.database().ref(fid).child(userid).set({
                                        email:user_email
                                    });
                                    femail.value = "";
                                    modal.style.display = "none";
                                }
                                else {
                                    alert('no such user');
                                    femail.value = "";
                                }
                            })
                        }
                    })
                }
            });
            //say something in lobby
            post_btn = document.getElementById('post_btn');
            post_txt = document.getElementById('comment');
            post_btn.addEventListener('click', function () {
                if (post_txt.value != "") {
                    var date=new Date();
                    var newpostref = firebase.database().ref('chat_list').push();
                    newpostref.set({
                        email: user_email,
                        data: post_txt.value,
                        time: date.toString()
                    });
                post_txt.value = "";
                }
            });
            var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
            var str_after_content = "</p></div></div>\n";
            var postsRef = firebase.database().ref('chat_list');
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            var temp=0;
            var amp=[];
            postsRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    var temp2 = temp.toString();
                    total_post[total_post.length] = str_before_username + childData.email + "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-dark'></strong>" + "<span id="+temp2+"></span>" + "</br></br></br>" + childData.time + str_after_content ;
                    first_count += 1;
                    amp[amp.length]=childData.data;
                    temp+=1;
                });
                document.getElementById('post_list').innerHTML = total_post.join('');
                for(i=0;i<temp;i++){
                    var temp2=i.toString();
                    document.getElementById(temp2).innerText=amp[i];
                }
                //add listener
                postsRef.on('child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var temp2=temp.toString();
                        var childData = data.val();
                        if(childData.email!=user_email){
                            notifyMe();
                        }
                        total_post[total_post.length] = str_before_username + childData.email + "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-dark'></strong>" + "<span id="+temp2+"></span>" + "</br></br></br>" +childData.time + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                        amp[amp.length]=childData.data;
                        temp+=1;
                        for(i=0;i<temp;i++){
                            var temp2=i.toString();
                            document.getElementById(temp2).innerText=amp[i];
                        }
                        
                    }
                });
            }).catch(e => console.log(e.message));
            //friend
            var friendmail = firebase.database().ref(userid);
            var total_friend = [];
            var str1 = '<button id="friendzone" value="';
            var str2 = '"onclick="myFunction(this)" >';
            var str_after_friendname = '</button>';
            var friend1_count = 0;
            var friend2_count = 0;
            friendmail.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var friendData = childSnapshot.val();
                    total_friend[total_friend.length] = str1 + friendData.email + str2 + friendData.email + str_after_friendname ;
                    friend1_count += 1;
                });
                document.getElementById('mwt_slider_content').innerHTML = total_friend.join('');
                //friend listener
                friendmail.on('child_added', function (data) {
                    friend2_count += 1;
                    if (friend2_count > friend1_count) {
                        var friendData = data.val();
                        total_friend[total_friend.length] = str1 + friendData.email + str2 + friendData.email + str_after_friendname ;
                        document.getElementById('mwt_slider_content').innerHTML = total_friend.join('');
                    }
                });
            })
            //好友私聊
            message_btn = document.getElementById('messagebtn');
            message_txt = document.getElementById('message');
            message_btn.addEventListener('click', function () {
                var friendtitle = document.getElementById('chatid');
                var friendid = friendtitle.textContent;
                chatfriend = friendtitle.textContent.replace(/\./g,"-");
                if (message_txt.value != "") {
                    var date = new Date();
                    var now = date.getFullYear()+'/'+(date.getMonth()+1)+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes();
                    var newpostref1 = firebase.database().ref(userid).child(chatfriend).push();
                    newpostref1.set({
                        email: user_email,
                        data: message_txt.value,
                        time: now
                    });
                    var newpostref2 = firebase.database().ref(chatfriend).child(userid).push();
                    newpostref2.set({
                        email: user_email,
                        data: message_txt.value,
                        time: now
                    });
                    message_txt.value = "";
                }
            });
        }
    });
}
window.onload = function () {
    initwindow();
}
