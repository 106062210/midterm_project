document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });

  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification('Friend zone', {
        body: "Join the friends every once in a while",
      });
  
      notification.onclick = function () {
        window.open("https://midterm2-9b486.firebaseapp.com/texting.html");      
      };
  
    }
  
  }