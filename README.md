# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : friend zone chatroom
* Key functions (add/delete)
    1. add friends (you have to know the id(email) of the person you want to befriend with to add him/her to friend)
    2. chat with online friends
    3. talk with everybody in lobby
    
* Other functions (add/delete)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm2-9b486.firebaseapp.com/

# Components Description : 
1. membership : simply using auth().signInWithEmailAndPassword(email, password),signInWithPopup(provider),auth().createUserWithEmailAndPassword(email,password)
    very similar to lab06.
2. Firebase Page : firebase deploy.
3. Database : Database saves users and their friends and what they say.
4. RWD: the website is still readable with smaller window.
5. Topic Key Function : Can add friends and chat with them, you can also talk to everybody through lobby.
6. CSS Animation : the title is jello.
7. Chrome Notification : asks user to join the chat when there's people logging on to friend zone.

...

# Other Functions Description(1~10%) : 
...

## Security Report (Optional)
